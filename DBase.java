import java.io.*;
import java.sql.*;

/**
 * Created by vtvpmc on 6/6/2017.
 */
public class DBase {
    final String CONFIG_FILENAME = "config.txt";
    String CONNECTION_STRING = "";
    String USER = "";
    String PASS = "";
    private static DBase instance = null;
    public static DBase getInstance()
    {
        if (instance == null)
            instance = new DBase();
        return instance;
    }

    private Connection co = null;
    public Connection getConnection()
    {
        return co;
    }

    private DBase ()
    {
        try
        {
            LoadConfigFile();
            Class.forName("com.mysql.jdbc.Driver");
            co = DriverManager.getConnection(CONNECTION_STRING, USER, PASS);
        } catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }

    private void LoadConfigFile() throws IOException
    {
        FileInputStream fis = new FileInputStream(CONFIG_FILENAME);
        BufferedReader br = new BufferedReader(new InputStreamReader(fis));
        CONNECTION_STRING = br.readLine();
        USER = br.readLine();
        PASS = br.readLine();
    }
}
