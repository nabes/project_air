import java.util.*;
import java.sql.*;
import java.sql.Date;

/**
 * Created by vtvpmc on 6/6/2017.
 */
public class Main {
    public int id;
    public String name;
    public String surname;
    public int age;
    public int weight;
    public  int coupon;
    public int flew;
    public String email;
    public String telephone;
    public String CurrentDate;

    @Override
    public String toString() {
        return "Main{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", weight=" + weight +
                ", coupon=" + coupon +
                ", flew=" + flew +
                ", email='" + email + '\'' +
                ", telephone='" + telephone + '\'' +
                ", date='" + CurrentDate + '\'' +
                '}';
    }
}
