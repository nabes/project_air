import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by vtvpmc on 6/6/2017.
 */
public class MainSQL {
    public boolean addData(Main main)
    {
        String query = "INSERT INTO Main (name, surname, age, weight, telephone, email, coupon, flew) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        try
        {
            PreparedStatement ps = DBase.getInstance().getConnection().prepareStatement(query, 1);
            ps.setString(1, main.name);
            ps.setString (2, main.surname);
            ps.setInt(3, main.age);
            ps.setInt(4, main.weight);
            ps.setString(5, main.telephone);
            ps.setString(6, main.email);
            ps.setInt(7, main.coupon);
            ps.setInt(8, main.flew);
            ps.execute();
            ResultSet rs = ps.getGeneratedKeys();
            if (!rs.next())
                return false;
            main.id = rs.getInt(1);
            return true;
        }
        catch (SQLException e)
        {
            System.out.println(e.getMessage());
            System.out.println(main);
            System.out.println(query);
        }
        return false;
    }

    public List<Main> getRecords (String sign)
    {
        ArrayList<Main> list = new ArrayList<Main>();
        String query = "SELECT * FROM main";
        Statement st;
        try
        {
            st = DBase.getInstance().getConnection().createStatement();
            ResultSet rs = st.executeQuery(query);
            while (rs.next())
            {
                Main main = new Main();
                main.id = rs.getInt(1);
                main.name = rs.getString(2);
                main.surname = rs.getString(3);
                main.age = rs.getInt(4);
                main.weight = rs.getInt(5);
                main.telephone = rs.getString(6);
                main.email = rs.getString(7);
                main.coupon = rs.getInt(8);
                main.flew = rs.getInt(9);
                main.CurrentDate = rs.getString(10);
                list.add(main);
            }
        }
        catch (SQLException e)
        {
            System.out.println(e.getMessage());
            System.out.println(query);
        }
        return list;
    }

    public boolean delData (Main main)
    {
        String query = "DELETE FROM Main WHERE id = ?";
        try
        {
            PreparedStatement ps = DBase.getInstance().getConnection().prepareStatement(query);
            ps.setInt(1, main.id);
            ps.execute();
        } catch (SQLException e)
        {
            System.out.println(e.getMessage());
            System.out.println(query);
        }
        return false;
    }
}
