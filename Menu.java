import com.sun.org.apache.xpath.internal.SourceTree;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
/**
 * Created by vtvpmc on 6/6/2017.
 */
public class Menu {
    Scanner scanner = new Scanner(System.in);
    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    Date date = new Date();

    public void show ()
    {
        System.out.println();
        System.out.println("=== MENU ===");
        System.out.println("1.  Add client");
        System.out.println("2.  Show clients");
        System.out.println("3.  Mark clients who have flown");
        System.out.println("0.  Exit");
    }

    public int getItem()
    {
        int item = 0;
        do {
            System.out.println();
            System.out.print("> ");
            item = scanner.nextInt();
            scanner.nextLine();
        }while (item < 0 || item > 3);
        return item;
    }

        public void start()
        {
            int item;
            do {
                show();
                item=getItem();
                switch(item)
                {
                    case 1 : doClientAdd(); break;
                    case 2 : doShowClient(); break;
                    case 3 : doDeleteClient(); break;
                }
            } while (item != 0);
            System.out.println("Bye");
        }

        private void doClientAdd()
        {
            Main main = new Main();
            MainSQL sql = new MainSQL();
            System.out.println("Add a new client");
            System.out.print("Name: ");
            System.out.println();
            main.name = scanner.nextLine();
            System.out.print("Surname: ");
            System.out.println();
            main.surname = scanner.nextLine();
            System.out.print("Age: ");
            System.out.println();
            main.age = scanner.nextInt();
            scanner.nextLine();
            System.out.print("Weight(kg): ");
            System.out.println();
            main.weight = scanner.nextInt();
            scanner.nextLine();
            System.out.print("Telephone: ");
            System.out.println();
            main.telephone = scanner.nextLine();
            System.out.print("Email: ");
            System.out.println();
            main.email = scanner.nextLine();
            System.out.print("Do you need a coupon? (1 - yes, 0 - no): ");
            System.out.println();
            main.coupon = scanner.nextInt();
            scanner.nextLine();
            main.CurrentDate = dateFormat.format(date);

            if (sql.addData(main))
                System.out.println("Added a new record" + main.id);
            else
                System.out.println("Error");
        }

        private void showList(List<Main> list)
        {
            System.out.println("id  | name | surname  | age  |  weight | telephone  |  email | coupon | flew | date |");
            System.out.println("----+------+------+---------+--------------+---------------");

            for (Main main : list)
                System.out.println(main.id + "\t" + "\t" +
                                    main.name + "\t" + "\t" +
                                    main.surname + "\t" + "\t" +
                                    main.age + "\t" + "\t" +
                                    main.weight + "\t" + "\t" +
                                    main.telephone + "\t" + "\t" +
                                    main.email + "\t" + "\t" +
                                    main.coupon + "\t" + "\t" +
                                    main.flew + "\t" +
                                    main.CurrentDate);
        }

        private void doShowClient()
        {
            System.out.println("=== Showing all clients ===");
            System.out.println();
            MainSQL sql = new MainSQL();
            showList (sql.getRecords(""));
        }

        private void doDeleteClient()
        {
            Main main = new Main();
            MainSQL sql = new MainSQL();
            System.out.println("Choose client to delete");
            System.out.print("id: ");
            main.id = scanner.nextInt();

            if (sql.delData(main))
                System.out.println("Deleted a record id " + main.id);
            else System.out.println("Error");
        }
}