/**
 * Created by vtvpmc on 6/6/2017.
 */
public class Program {
    public static void main(String[] args)
    {
        try
        {
            Menu menu = new Menu();
            menu.start();
        } catch (Exception e)
        {
            System.out.println("Fatal error : ");
            System.out.println(e.getMessage());
        }
    }
}